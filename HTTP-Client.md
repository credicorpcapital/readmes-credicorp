# HTTP Client


## Introduction

- La capacidad de HTTP Client hace referencia a la capacidad del framework de poder consumir otros microservicios de una forma simple y rápida.

## How to use
- Cuando un microservicio “A”, quiere consumir información de un microservicio “B”, el desarrollador no debe implementar ninguna clase o configuración, solo debe hacer uso de la clase cliente autogenerada.

```
@RetrofitClient(
  name = "CustomersApi",
  url = "${credicorpcapital.http-client.api.client-0.base-url:http://localhost:8081/v1/}"
)
public interface CustomersApi {
  @Headers({"Content-Type: application/json", "Accept: application/json"})
  @POST("customers")
  Mono<CreatedCustomerRs> createCustomer(@HeaderMap Map<String, String> var1, @Body CreateCustomerRq var2);
...
}
```

## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```
<dependencies>
  <dependency>
      <groupId>com.squareup.retrofit2</groupId>
      <artifactId>retrofit</artifactId>
  </dependency>
</dependencies>
```

