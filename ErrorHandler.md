# Error Handler


## Introduction

- La capacidad de error handler se basa en mantener una estructura uniforme del manejo de excepciones, la cual el desarrollador pueda personalizar al momento de arrojar la excepción

## How to use
- En caso de que se quiera arrojar una excepción cuando un request no está autorizado o viene con una cabecera obligatoria en blanco, se arrojara una excepción de tipo ApiException, la cual será manejada por el framework.

```
if (ocpApimSubscriptionKey.isEmpty()) {
      log.error("Unauthorized Request, no Apim Subscription Key");
      ApiException ex = ApiException.builder()
              .category(UNAUTHORIZED)
              .code("CCC-001")
              .detail(ExceptionDetail.builder()
                      .code("CCC-001")
                      .argument("")
                      .message("Usted no está autorizado")
                      .component("apidemo")
                      .build())
              .build();
      throw ex;
    }
```

## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```

```

