# Pruebas Unitarias


## Introduction

- La capacidad del framework para pruebas unitarias pasa por importación de todas las librerías necesarias para que el desarrollador pueda realizar las pruebas sin preocuparse por las dependencias (junit, easymock, assertjk, etc)

## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```
<dependencies>
    <reactor-test.version>3.4.3</reactor-test.version>
    <spock.version>1.3-groovy-2.5</spock.version>
    <podam.version>7.2.6.RELEASE</podam.version>
    <easymock.version>4.2</easymock.version>
    <powermock.version>2.0.9</powermock.version>
    <jsonwebtoken-jjwt.version>0.9.1</jsonwebtoken-jjwt.version>
    <nimbus-jose-jwt.version>8.20.1</nimbus-jose-jwt.version>
    <bouncycastle.version>1.68</bouncycastle.version>
    <cucumber-jvm.version>6.9.1</cucumber-jvm.version>
    <cucumber-reporting.version>5.4.0</cucumber-reporting.version>
    <hoverfly-java.version>0.13.1</hoverfly-java.version>
    <mock-sever-netty-version>5.11.2</mock-sever-netty-version>
    <embedded-redis.version>0.7.3</embedded-redis.version>
    <kafka-junit5.version>3.2.2</kafka-junit5.version>
    <zookeeper.version>3.5.9</zookeeper.version>
    <karate-bdd.version>0.9.6</karate-bdd.version>
    <openapi-diff.version>1.2.0</openapi-diff.version>
    <junit5.version>5.7.1</junit5.version>
    <fabric8.version>4.10.0</fabric8.version>
    <jmh.version>1.27</jmh.version>
    <json.version>20201115</json.version>
    <rest-assured.version>4.3.3</rest-assured.version>
    <awaitility.version>4.0.3</awaitility.version>
    <retrofit.version>2.9.0</retrofit.version>
    <groovy-all.version>3.0.7</groovy-all.version>
    <assertj.version>3.19.0</assertj.version>
    <logback.version>1.2.3</logback.version>
</dependencies>
```

