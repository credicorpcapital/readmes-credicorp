# Logging


## Introduction

- Módulo que se encarga de proporcionar una forma de enriquecer los mensajes de logging con información que podría no estar disponible en el ámbito en el que realmente se produce el log, pero que puede ser útil para realizar un mejor seguimiento de la ejecución del programa.

## How to use

- Cuando se quiere tener la trazabilidad de ciertos request, con la capacidad del logging, se puede imprimir en consola o donde la configuración lo indique 


## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```
<dependencies>
   <dependency>
      <groupId>io.projectreactor.addons</groupId>
      <artifactId>reactor-extra</artifactId>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
    </dependency>
    <dependency>
      <groupId>org.assertj</groupId>
      <artifactId>assertj-core</artifactId>
    </dependency>
</dependencies>
```

