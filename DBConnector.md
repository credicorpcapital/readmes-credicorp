# DB Connector


## Introduction

- El generador agrega las siguientes dependencias cuando se selecciona la propiedad de SQL Server, de cara a que el desarrollador solo tenga que reemplazar las propiedades de conexión
- Las dependencias permiten una conexión a Azure SQL con autenticación por Service Principal.


## How to use
- Se debe modificar las propiedades de conexión
```
url: jdbc:sqlserver://asql-ds-gda-eu2-sss-001.database.windows.net:1433;databaseName=sqldb-api-demo;encrypt=true;loginTimeout=30
username: 4a4a4f4c-1e05-4c1f-89bf-fd7d1b686972
password: .ga8Q~XoPn18AMwua4.AtPj~2qVh4oSse-pnGbo6
```

## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
    <groupId>com.microsoft.sqlserver</groupId>
    <artifactId>mssql-jdbc</artifactId>
    <version>10.2.1.jre8</version>
</dependency>
<dependency>
    <groupId>com.microsoft.azure</groupId>
    <artifactId>msal4j</artifactId>
    <version>1.12.0</version>
</dependency>
```

