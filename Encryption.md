# Encryption


## Introduction

- El módulo de Encryption proporciona soporte para cifrado simétrico, generación de claves y codificación de contraseñas. 
- Las interfaces permiten implementar la fuente de cifrado deseada según la casuistica del negocio
- Los metodos encrypt y decrypt se implementan de manera reactiva

## How to use
- En el caso de que se esté haciendo una operación en donde se este guardando datos sensibles, el desarrollador puede hacer uso de esta capacidad para poder encriptar el campo encriptado dentro de una base de datos.
Ejm. Claves, datos personales, número de tarjeta, etc.

```
SpringEncryptionOperation springEncryptionOperation =
            new SpringEncryptionOperation("70617373776f7264", "73616c74");

return requestBody
            .flatMap(request -> springEncryptionOperation
                    .encrypt(request.getPassword())
                    .map(passwordEncryptedText -> {
                      return request.password(passwordEncryptedText);
                    }))
```

## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-crypto</artifactId>
</dependency>
```

