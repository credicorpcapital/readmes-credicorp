# Validator


## Introduction

- La capacidad de validación del framework está orientada a la autogeneración de restricciones y validadores de parámetros en base a un contrato OpenApi. Esto le permite al desarrollador dejar las validaciones requeridas del contrato en manos del framework

## How to use
- Como se puede ver en el ejemplo a continuación en el contrato OpenApi se esta restringiendo a que el campo correo, no este vacío y siga cierto patrón.

```
EmailFrom:
  title: Responsable
  required:
    - email
  type: object
  properties:
    email:
      pattern: ^[\w!#$%&'*+/=?`{|}~^-]+(?:\.[\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$
      type: string
      description: La dirección de correo electrónico 'De' utilizada para enviar el mensaje. Esta dirección debe ser un remitente verificado en su cuenta Twilio SendGrid.
      example: alguien@hotmail.com
```
Código generado
```
  @ApiModelProperty(
    example = "alguien@hotmail.com",
    required = true,
    value = "La dirección de correo electrónico 'De' utilizada para enviar el mensaje. Esta dirección debe ser un remitente verificado en su cuenta Twilio SendGrid."
  )
  public @NotNull @Pattern(
  regexp = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"
) String getEmail() {
    return this.email;
  }
```

## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```
<dependencies>
    <dependency>
        <groupId>com.credicorpcapital.it.archit.fmwk.ms</groupId>
        <artifactId>credicorpcapital-openapi-generator</artifactId>
        <version>1.0.2</version>
    </dependency>
</dependencies>
```

