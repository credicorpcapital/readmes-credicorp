# Circuit Breaker


## Introduction

- Circuit breaker es la capacidad que nos va a permitir hacer a nuestros microservicios resilientes. La capacidad está orientada a programación funcional, es de fácil uso y ligera.

## How to use

- Cuando se quiere realizar una operación a base de datos y esta no responde.
En este caso se define un método de fallback en el cual se especifica cual será la acción ante la falta de respuesta.

```
.map(notificacionRepository::save)
            .transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
            .doOnError(cust -> {
              ApiException ex = ApiException.builder()
                      .category(ErrorCategory.FORBIDDEN)
                      .code("P0001")
                      .message("Circuit breaker").build();
              throw ex;
            })
            .map(resp -> new SendGridResponse()
                    .status(PROCESSED)
                    .noticationId(resp.getId())
            )
```

## Dependencies
- Las dependencias que usa la capacidad son las siguientes:
```
<dependencies>
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
    <version>2.7.0</version>
  </dependency>
  <dependency>
    <groupId>io.github.resilience4j</groupId>
    <artifactId>resilience4j-spring-boot2</artifactId>
    <version>1.6.1</version>
  </dependency>
  <dependency>
    <groupId>io.github.resilience4j</groupId>
    <artifactId>resilience4j-reactor</artifactId>
    <version>1.6.1</version>
  </dependency>
</dependencies>
```

